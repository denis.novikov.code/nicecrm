from django.urls import path
from .views import MainPageView, CompanyList, ProjectList, ProjectDetail, InteractionList, InteractionDetail, \
    InteractionCreateForm, InteractionUpdateForm, InteractionDeleteForm, CompanyCreateForm, ProjectCreateForm, \
    ProjectUpdateForm, ProjectDeleteForm, CompanyDeleteForm, CompanyUpdateForm, CompaniesInteractionList, \
    ProjectsInteractionList, UpdateProfile, UserPasswordChangeView

urlpatterns = [
    path('profile/', UpdateProfile.as_view(), name='profile'),
    path('profile/password_change/', UserPasswordChangeView.as_view(), name='password_change'),
    path('interactions/interaction/<int:pk>/delete', InteractionDeleteForm.as_view(), name='delete-interaction'),
    path('interactions/interaction/<int:pk>/update', InteractionUpdateForm.as_view(), name='update-interaction'),
    path('interactions/interaction/<int:pk>/add', InteractionCreateForm.as_view(), name='add-interaction'),
    path('interactions/interaction/<int:pk>', InteractionDetail.as_view(), name='interaction'),
    path('interactions/', InteractionList.as_view(), name='interactions'),
    path('projects/project/add', ProjectCreateForm.as_view(), name='add-project'),
    path('projects/project-interactions/<int:project_id>', ProjectsInteractionList.as_view(), name='project-interaction-list'),
    path('projects/project/<int:pk>/delete', ProjectDeleteForm.as_view(), name='delete-project'),
    path('projects/project/<int:pk>/update', ProjectUpdateForm.as_view(), name='update-project'),
    path('projects/project/<int:pk>', ProjectDetail.as_view(), name='project'),
    path('projects/', ProjectList.as_view(), name='projects'),
    path('companies/company-interactions/<int:company_id>', CompaniesInteractionList.as_view(), name='company-interaction-list'),
    path('companies/company-projects/<int:company_id>', ProjectList.as_view(), name='company-project-list'),
    path('companies/company/<int:pk>/delete', CompanyDeleteForm.as_view(), name='delete-company'),
    path('companies/company/<int:pk>/update', CompanyUpdateForm.as_view(), name='update-company'),
    path('companies/company/add', CompanyCreateForm.as_view(), name='add-company'),
    path('companies/', CompanyList.as_view(), name='companies'),
    path('', MainPageView.as_view(), name='home'),
]