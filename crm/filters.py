import django_filters

from crm.models import Interaction


class InteractionFilter(django_filters.FilterSet):

    project__icontains = django_filters.CharFilter(label='Поиск по проекту', field_name='project__name', lookup_expr='icontains')
    company__icontains = django_filters.CharFilter(label='Поиск по компании', field_name='project__company__name', lookup_expr='icontains')


    class Meta:
        model = Interaction
        fields = ['chanel', 'manager', 'rate']