from django.contrib import admin

from .models import Telephone, Email, Company, Project, Interaction, Profile


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'contact_person',  'created_at', 'updated_at')
    list_display_links = ('name',)
    search_fields = ('name', 'contact_person')


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'company', 'cost', 'started_at', 'finish_at')
    list_display_links = ('name', 'company')
    search_fields = ('name', 'company')


class InteractionAdmin(admin.ModelAdmin):
    list_display = ('project', 'chanel', 'rate', 'created_at', 'updated_at')
    list_display_links = ('project',)
    search_fields = ('project__name',)

admin.site.register(Profile)
admin.site.register(Email)
admin.site.register(Telephone)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Interaction, InteractionAdmin)
# Register your models here.
