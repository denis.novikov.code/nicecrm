import PIL
from django.contrib.auth.models import User
from django.core import validators
from django.db import models
from PIL import Image


class Company(models.Model):
    name = models.CharField(
        max_length=64,
        db_index=True,
        verbose_name='Название компании',
        unique='True',
    )

    contact_person = models.CharField(
        max_length=64,
        verbose_name='Полное имя контактного лица',
    )

    short_description = models.CharField(
        max_length=256,
        verbose_name='Короткое описание',
        blank=True,
    )

    adress = models.CharField(
        max_length=128,
        verbose_name='Адресс компании',
        blank=True
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Компании'
        verbose_name = 'Компания'
        ordering = ['name']


class Telephone(models.Model):
    number = models.CharField(
        max_length=20,
        db_index=True,
        verbose_name='Телефон',
        help_text='Введите номер телефона',
        validators=[validators.RegexValidator(regex='^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')],
    )

    company = models.ForeignKey(
        Company,
        db_index=True,
        verbose_name='Компания',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.number

    class Meta:
        verbose_name_plural = 'Телефоны'
        verbose_name = 'Телефон'


class Email(models.Model):
    email = models.EmailField(
        db_index=True,
        verbose_name='Email',
        help_text='Введите email',
        unique=True,
    )
    company = models.ForeignKey(
        Company,
        db_index=True,
        verbose_name='Компания',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.email

    class Meta:
        verbose_name_plural = 'Email-ы'
        verbose_name = 'Email'


class Project(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name='Название проекта',
        unique=True,
        db_index=True,
    )

    description = models.TextField(
        verbose_name='Описание проекта',
        blank=True,
        unique=True,
        max_length=2048
    )

    started_at = models.DateField(
        verbose_name='Дата начала проекта',
        null=False,
    )

    finish_at = models.DateField(
        verbose_name='Дата окончания проекта',
        null=True,
        blank=True,
    )

    cost = models.IntegerField(
        verbose_name='Стоимость проекта',
        null=True,
        blank=True,
        validators=[validators.MinValueValidator(0)]
    )

    company = models.ForeignKey(
        Company,
        verbose_name='Компания заказчик',
        db_index=True,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Проекты'
        verbose_name = 'Проект'
        ordering = ['-started_at']

    def __str__(self):
        return self.name


class Interaction(models.Model):
    project = models.ForeignKey(
        Project,
        verbose_name='Проект',
        db_index=True,
        on_delete=models.CASCADE
    )

    CHANNELS = (
        ('r', 'Заявка'),
        ('l', 'Письмо'),
        ('w', 'Вебсайт'),
        ('i', 'Входящее письмо'),
        ('c', 'Инициатива компании'),
    )

    chanel = models.CharField(
        max_length=1,
        choices=CHANNELS,
        verbose_name='Канал связи',
    )

    description = models.TextField(
        max_length=2048,
        blank=True,
        verbose_name='Описание',
    )

    RATES = (
        ('1', 'Ужастно'),
        ('2', 'Плохо'),
        ('3', 'Нормально'),
        ('4', 'Хорошо'),
        ('5', 'Отлично')
    )
    rate = models.CharField(
        max_length=1,
        choices=RATES,
        blank=True,
        verbose_name='Оценка',
    )

    manager = models.ForeignKey(
        User,
        verbose_name='Менеджер',
        on_delete=models.CASCADE,
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Взаимодействия'
        verbose_name = 'Взаимодействие'
        ordering = ['-updated_at']

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField()


