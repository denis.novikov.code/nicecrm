from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
from django.forms import ModelForm, Form
from django_summernote.widgets import SummernoteWidget

from .models import Interaction, Company, Project, Telephone, Email, Profile


class InteractionForm(ModelForm):
    class Meta:
        model = Interaction
        fields = ('chanel', 'description', 'rate')


class CompanyForm(ModelForm):
    class Meta:
        model = Company
        fields = ('name', 'contact_person', 'short_description', 'adress')
        widgets = {
            'short_description': SummernoteWidget(),
        }

class TelephoneForm(ModelForm):
    class Meta:
        model = Telephone
        fields = ('number',)


class EmailForm(ModelForm):
    class Meta:
        model = Email
        fields = ('email',)

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'company', 'description', 'cost', 'started_at', 'finish_at')
        widgets = {
            'description': SummernoteWidget(),
        }

class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name','email')

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)

    def clean_avatar(self):
        avatar = self.cleaned_data['avatar']

        try:
            w, h = get_image_dimensions(avatar)

            max_width = max_height = 2000
            if w > max_width or h > max_height:
                print(w,h)
                raise ValidationError(
                    f'Пожалуйста, используйте изображение размером '
                    f'{max_width} x {max_height} пикелей или меньше.')

            main, sub = avatar.content_type.split('/')
            if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
                raise ValidationError('Пожалйста используйте форматы JPEG, GIF или PNG.')

            # validate file size
            if len(avatar) > (1024 * 1024):
                raise ValidationError(f'Аватар не должен быть больше 20mb')

        except AttributeError:
            """
            Handles case when we are updating the user profile
            and do not supply a new avatar
            """
            pass

        return avatar