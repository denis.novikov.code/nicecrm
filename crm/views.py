from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordChangeView
from django.core.cache import cache
from django.core.paginator import Paginator
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic.base import TemplateView, ContextMixin
from django.views.generic.edit import DeleteView, CreateView, UpdateView

from .filters import InteractionFilter
from .forms import InteractionForm, CompanyForm, ProjectForm, EmailForm, TelephoneForm, UserForm, ProfileForm
from .models import Company, Project, Interaction, Email, Telephone


class MenuMixin(ContextMixin):
    """ Класс для расширения конекста данными для генерации  в хедере сайта меню

    Наследует:
        ContextMixin - класс джанго для работы с контекстом

    Содержит только один метод который переопределяет ''get_context_data'' родительского класса.
    """

    def get_context_data(self, **kwargs):
        """ Переопределеный метод для добавление в контекст словарей с данными для хедера

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)

        # Создание словаря для пунктов топ-меню
        context['menu'] = [
            {'anchor': 'Компании', 'href': reverse('companies'),
             'count': cache.get_or_set('category_count', Company.objects.count(), 60)},
            {'anchor': 'Проекты', 'href': reverse('projects'),
             'count': cache.get_or_set('project_count', Project.objects.count(), 60)},
            {'anchor': 'Взаимодействия', 'href': reverse('interactions'),
             'count': cache.get_or_set('interaction_count', Interaction.objects.count(), 60)}
        ]

        # Создание словаря для пунктов бот-меню
        context['functions'] = [
            {'anchor': 'Добавить Команию', 'href': reverse('add-company')},
            {'anchor': 'Добавить Проект', 'href': reverse('add-project')},
        ]
        return context


class MainPageView(LoginRequiredMixin, MenuMixin, TemplateView):
    """ Класс для обработки запроса главной страницы.

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        TemplateView - класс джанго, для наследования общих функций и функцию отображения шаблона

    Atributes:
        template_name - переменная содержащая название шаблона для отображения


    """
    template_name = 'crm/home.html'

    def get(self, request, *args, **kwargs):
        """Переопределенный метод обрабатывающий get запрос для получения главной страницы

        При обработке метод учитывает пагинацию, сортировку

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        # НАЧАЛО проверки номера страницы
        if 'page' in request.GET:
            page_num = self.request.GET['page']
        else:
            page_num = 1

        # Сохранение номера страницы для Paginator при работе с контекстом
        kwargs.update({'page_num': page_num})
        # КОНЕЦ проверки номера страницы

        # НАЧАЛО проверки применения сортировки
        if 'order_by' in request.GET:
            kwargs.update({'order_by': request.GET['order_by']})
        # КОНЕЦ проверки применения сортировки

        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        """Переопределенный метод для работы с контекстом с учетом пагинации и сортировки


        :param kwargs:
        :return:
        """

        # Словарь допустимых сортировок в формате "название сортировки" : "направление сортировки"
        sort_list = {'sort': 'order_by', 'name': '-name', 'name_rev': 'name', 'created_at': 'created_at',
                     'created_at_rev': '-created_at'}

        context = super().get_context_data(**kwargs)

        context['sort_list'] = sort_list

        # Получаем все компании + телефоны и email
        companies = Company.objects.prefetch_related('email_set', 'telephone_set').all()

        # НАЧАЛО проверка применения сортировки
        if kwargs.get('order_by'):
            context['order_by'] = f"&order_by={kwargs['order_by']}"
            companies = companies.order_by(kwargs['order_by'])
        # КОНЕЦ проверка применения сортировки

        # НАЧАЛО Создание пагинации
        paginator = Paginator(companies, 3, 0)
        page = paginator.get_page(kwargs['page_num'])
        # КОНЕЦ Создание пагинации

        # Добавление в контекст одной определенной страницы
        context['companies'] = page
        # Добавление в контекст общего количества записей
        context['companiesCount'] = companies.count()
        return context


class CompanyList(LoginRequiredMixin, MenuMixin, ListView):
    """Класс для обработки запроса получения списка компаний

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        ListView - класс джанго, для наследования функций для упращенной работы с списком объектов определенной модели

    Atributes:
        model - указываеться класс модель список объекта которого хотим получить
        context_object_name - указываеться, под каким названием будет храниться список объектов в контексте
        queryset - переменная в которой храниться параметр выборки из БД

    """
    model = Company
    context_object_name = 'companies'
    queryset = Company.objects.prefetch_related('email_set', 'telephone_set')


class CompanyCreateForm(LoginRequiredMixin, MenuMixin, CreateView):
    """Класс для обработки запроса на создании нового объекта модели Company

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        CreateView - класс джанго, для наследования функций для упращенной работы с формой создания объекта модели

    Atributes:
        template_name - переменная содержащая название шаблона для отображения
        form_class - переменная для хранения класса формы работающая с моделью Company
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном сохранении формы
        email_form - переменная для хранения класса формы работающая с связанной с моделью Company моделью Email
        telephone_form - переменная для хранения класса формы работающая с связанной с моделью Company моделью Telephone
    """
    template_name = 'crm/company_form.html'
    form_class = CompanyForm
    success_url = reverse_lazy('companies')
    email_form = None
    telephone_form = None

    def form_valid(self, form):
        """Переопределении метода валидации формы для учета связанных моделей Telephone и Email

        Валидация считаеться успешной если валидными являються все отправленные формы

        :param form:
        :return:
        """

        # НАЧАЛО Получение из контекста данные отправленных форм связанных моделей
        context = self.get_context_data()
        email_form = context['email_form']
        telephone_form = context['telephone_form']
        # КОНЕЦ Получение из контекста данные отправленных форм связанных моделей

        if email_form.is_valid() and telephone_form.is_valid() and form.is_valid():

            # Сохранение в базе и переменной нового объекта Company
            obj = form.save()

            # НАЧАЛО Сохранение нового обїекта модели Email
            email = self.email_form.save(commit=False)
            email.company = obj
            email.save()
            # КОНЕЦ Сохранение нового объекта модели Email

            # НАЧАЛО Сохранение нового объекта модели Telephone
            telephone = self.telephone_form.save(commit=False)
            telephone.company = obj
            telephone.save()
            # НАЧАЛО Сохранение нового объекта модели Telephone

            messages.success(self.request, 'Компания успешно созданна!')
            return redirect(self.success_url)
        else:
            return self.render_to_response(
                self.get_context_data(form=form, email_form=email_form, telephone_form=telephone_form))

    def get(self, request, *args, **kwargs):
        """Обработка запроса получение страницы создания Компании

        Метод создаёт формы для связанных с моделю Company, моделей Email и Telephone

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.email_form = EmailForm(prefix='email')
        self.telephone_form = TelephoneForm(prefix='telephone')

        return super(CompanyCreateForm, self).get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Обработка запроса создания объекта модели Company и связанных моделей Email и Telephone посредством форм

        Запоминат данные отправленных форм для дальнейшей валидации

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.email_form = EmailForm(request.POST, prefix='email')
        self.telephone_form = TelephoneForm(request.POST, prefix='telephone')

        return super().post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Переопределение метода для работы с контекстом

        Добавляет в контекст объекты формы связанных моделей для отображении их на шаблоне
        Добавляет Текст который будет дополнять тег h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Создать '
        context['email_form'] = self.email_form
        context['telephone_form'] = self.telephone_form
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе

        :return:
        """
        messages.success(self.request, 'Компания успешно созданна!')
        return super(CompanyCreateForm, self).get_success_url()


class CompanyDeleteForm(LoginRequiredMixin, DeleteView):
    """Класс для обработки запросов на удаление объектов класса Company

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        DeleteView - класс джанго, для наследования функций для упращенной работы с формой удаления объекта модели

    Atributes:
        model - класс модели удаляемого объекта
        template_name - переменная содержащая название шаблона для отображения
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном результате работы формы


    """
    model = Company
    template_name = 'crm/delete_form.html'
    success_url = reverse_lazy('companies')

    def get_context_data(self, **kwargs):
        """Переопределнный метод для работы с контекстом

        Добавляет в контекст возвратную ссылку

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['return'] = reverse('companies')  # Как добавить ссылку назад на страницу
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе

        :return:
        """
        messages.success(self.request, 'Компания успешно удалена!')
        return super(CompanyDeleteForm, self).get_success_url()


class CompanyUpdateForm(LoginRequiredMixin, MenuMixin, UpdateView):
    """ Класс для обработки запросов на обновление объекта модели Company и связанных с ним Email, Telephone

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        UpdateView - класс джанго, для наследования функций для упращенной работы с формой изменения объекта модели

    Atributes:
        template_name - переменная содержащая название шаблона для отображения
        form_class - переменная для хранения класса формы работающая с моделью Company
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном результате работы формы
        email_form - переменная для хранения класса формы работающая с связанной с моделью Company моделью Email
        telephone_form - переменная для хранения класса формы работающая с связанной с моделью Company моделью Telephone

    """
    model = Company
    template_name = 'crm/company_form.html'
    form_class = CompanyForm
    success_url = reverse_lazy('companies')
    email_form = None
    telephone_form = None

    def form_valid(self, form):
        """Переопределении метода валидации формы для учета связанных моделей Telephone и Email
        Валидация считаеться успешной если валидными являються все отправленные формы

        :param form:
        :return:
        """

        email_form = self.email_form
        telephone_form = self.telephone_form

        if email_form.is_valid() and telephone_form.is_valid() and form.is_valid():

            # Сохранение изменений объекта Company
            form.save()

            # Сохранение изменений объекта Email
            email_form.save()

            # Сохранение изменений объекта Telephone
            telephone_form.save()

            messages.success(self.request, 'Компания успешно обновленна!')
            return redirect(self.success_url)
        else:
            return self.render_to_response(
                self.get_context_data(form=form, email_form=email_form, telephone_form=telephone_form))

    def get(self, request, *args, **kwargs):
        """Обработка запроса получение страницы изменения Компании

        Метод создаёт формы для связанных с моделю Company, моделей Email и Telephone из существующих объектов

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        company = Company.objects.get(pk=kwargs['pk'])

        # НАЧАЛО Инициализация формы из существующего объекта Email
        email = company.email_set.all()[0]
        self.email_form = EmailForm(instance=email, prefix='email')
        # КОНЕЦ Инициализация формы из существующего объекта Email

        # НАЧАЛО Инициализация формы из существующего объекта Telephone
        telephone = company.telephone_set.all()[0]
        self.telephone_form = TelephoneForm(instance=telephone, prefix='telephone')
        # КОНЕЦ Инициализация формы из существующего объекта Telephone

        return super(CompanyUpdateForm, self).get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """Обработка запроса на изменение объектов Company, Email, Telephone с помощью отправленных форм

        Метод создает формы из существующих объектов и изменёных данных объектов формами

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        email = Email.objects.get(company_id=kwargs['pk'])
        self.email_form = EmailForm(request.POST, prefix='email', instance=email)

        telephone = Telephone.objects.get(company_id=kwargs['pk'])
        self.telephone_form = TelephoneForm(request.POST, prefix='telephone', instance=telephone)

        return super().post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Переопределение метода для работы с контекстом

        Добавляет в контекст объекты формы связанных моделей для отображении их в шаблоне
        Добавляет Текст который будет дополнять тег h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Изменить '
        context['email_form'] = self.email_form
        context['telephone_form'] = self.telephone_form

        return context


class CompaniesInteractionList(LoginRequiredMixin, MenuMixin, ListView):
    """Класс отвечающий за отображение списка взаимодействий по компании

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        ListView - класс джанго, для наследования функций для упращенной работы с списком объектов определенной модели

    Atributes:
        model - указываеться класс модель список объекта которого хотим получить
        context_object_name - указываеться, под каким названием будет храниться список объектов в контексте

    """
    model = Interaction
    context_object_name = 'interactions'

    def get_queryset(self):
        """Переопределение метода для получение списка взаимодействий отфильтрованного по компании

        :return:
        """
        if self.kwargs.get('company_id'):
            return Interaction.objects.filter(project__company=self.kwargs['company_id'])
        return Interaction.objects.all()

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['page_name'] = "Взаимодействия по компании"
        return context


class ProjectList(LoginRequiredMixin, MenuMixin, ListView):
    """Класс отвечающий за отображение списка проектов по компании или всех

        Наследует:
            LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
            MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
            ListView - класс джанго, для наследования функций для упращенной работы с списком объектов
            определенной модели

        Atributes:
            context_object_name - указываеться, под каким названием будет храниться список объектов в контексте

        """

    context_object_name = 'projects'

    def get_queryset(self):
        """Переопределение метода для получение списка проектов отфильтрованного по компании или всех

        :return:
        """
        if self.kwargs.get('company_id'):
            self.kwargs['page_name'] = "Проекты компании"
            return Project.objects.filter(company=self.kwargs['company_id'])
        self.kwargs['page_name'] = "Все проекты"
        return Project.objects.all()

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['page_name'] = self.kwargs['page_name']
        return context


class ProjectDetail(LoginRequiredMixin, MenuMixin, DetailView):
    """Класс для обработки запросов детального отображения проекта

    Наследует:
            LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
            MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
            DetailView - класс джанго, для наследования функций для упращенной работы с одним объектом модели

    Atributes:
        model - указываеться класс модель список объекта которого хотим получить
        queryset - указываем выборку объектов Project вместе связанными Interactions
        form_class - указываеться класс формы для работы с моделью Interaction
        form - переменная которая будет содержат форму

    """
    model = Project
    queryset = Project.objects.prefetch_related('interaction_set').order_by('-create_at')
    form_class = InteractionForm
    form = None

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """

        context = super().get_context_data(**kwargs)
        context['page_name'] = "Страница проекта"
        context['form'] = self.form_class()
        return context

    def post(self, request, *args, **kwargs):
        """Переопределенный метод для обработки запросов на создание взаимодействия посредством формы

        После успешной валидации формы, дополняет созданный на её основе объект даннымы о пользователе и проекте
        к которому принадлежит

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.form = self.form_class(request.POST)
        if self.form.is_valid():

            # Черновое сохранение нового объекта взаимодействия
            interaction = self.form.save(commit=False)

            # Дополнение нового объекта взаимодействия данными о проекте к которому он относиться
            interaction.project = Project.objects.get(pk=self.kwargs['pk'])

            # Дополнение нового объекта взаимодействия данными о пользователе которым был создан
            interaction.manager = User.objects.get(id=request.user.pk)

            # Сохранение в базу
            interaction.save()

            messages.success(self.request, 'Взаимодействие успешно созданно!')
            return redirect(reverse('project', kwargs={'pk': interaction.project.pk}))
        else:
            return super(ProjectDetail, self).get(request, *args, **kwargs)


class ProjectCreateForm(LoginRequiredMixin, MenuMixin, CreateView):
    """Класс для обработки запросов на создание объектов модели Project

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        CreateView - класс джанго, для наследования функций для упращенной работы с формой создания объекта модели

    Atributes:
        template_name - переменная содержащая название шаблона для отображения
        form_class - переменная для хранения класса формы работающая с моделью Company

    """
    template_name = 'crm/project_form.html'
    form_class = ProjectForm

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Создать '
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе работы с формой и возврат на страницу Проекта

        :return:
        """
        messages.success(self.request, 'Проект успешно создан!')
        pk = self.object.pk  # Как это засунуть в кварг на след. строке?
        return reverse_lazy('project', kwargs={'pk': pk})


class ProjectUpdateForm(LoginRequiredMixin, MenuMixin, UpdateView):
    """Класс для обработки запросов на создание объектов модели Project

        Наследует:
            LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
            MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
            UpdateView - класс джанго, для наследования функций для упращенной работы с формой изменения объекта модели

        Atributes:
            model - указываеться класс модель объекта которого хотим обновить
            form_class - переменная для хранения класса формы работающая с моделью Project

        """
    model = Project
    form_class = ProjectForm

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Изменить '
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе работы с формой и возврат на страницу Проекта

        :return:
        """
        messages.success(self.request, 'Проект успешно изменен!')
        pk = self.object.pk  # Как это засунуть в кварг на след. строке?
        return reverse_lazy('project', kwargs={'pk': pk})


class ProjectDeleteForm(LoginRequiredMixin, DeleteView):
    """Класс предназанчений для обработки запросов на удаление объектов модели Project

    Atributes:
        model - класс модели удаляемого объекта
        template_name - переменная содержащая название шаблона для отображения
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном результате работы формы

    """
    model = Project
    template_name = 'crm/delete_form.html'
    success_url = reverse_lazy('projects')

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет возвратную ссылку на страницу Проекта

        :param kwargs:
        :return:
        """

        pk = self.object.pk  # Как это засунуть в кварг на след. строке?
        context = super().get_context_data(**kwargs)
        context['return'] = reverse_lazy('project', kwargs={'pk': pk})  # Как добавить ссылку назад
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе работы с формой и возврат на страницу Проектов

        :return:
        """
        messages.success(self.request, 'Проект успешно удален!')
        return super(ProjectDeleteForm, self).get_success_url()


class ProjectsInteractionList(LoginRequiredMixin, MenuMixin, ListView):
    """Класс для обработки запросов на отображение взаимодействий по проекту
    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        ListView - класс джанго, для наследования функций для упращенной работы с списком объектов
        определенной модели

    Atributes:
        context_object_name - указываеться, под каким названием будет храниться список объектов в контексте
        model - указываеться класс модель список объектов которого хотим получить

    """
    model = Interaction
    context_object_name = 'interactions'

    def get_queryset(self):
        """Переопределение метода для получение списка проектов отфильтрованного по проекту

        :return:
        """
        if self.kwargs.get('project_id'):
            return Interaction.objects.filter(project_id=self.kwargs['project_id'])
        return Interaction.objects.all()

    def get_context_data(self, *args, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(*args, **kwargs)
        context['page_name'] = "Взаимодействия по проекту"
        return context


class InteractionList(LoginRequiredMixin, MenuMixin, ListView):
    """Класс для обработки запросов на отображение взаимодействий

        Наследует:
            LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
            MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
            ListView - класс джанго, для наследования функций для упращенной работы с списком объектов
            определенной модели

        Atributes:
            context_object_name - указываеться, под каким названием будет храниться список объектов в контексте
            model - указываеться класс модель список объектов которого хотим получить
            template_name - переменная содержащая название шаблона для отображения

        """
    model = Interaction
    context_object_name = 'interactions'
    template_name = 'crm/interaction_list_filtred.html'

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст форму фильтров
        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['page_name'] = "Все взаимодействия"

        # Добавление формы фильтров
        context['filter'] = InteractionFilter(self.request.GET, queryset=self.get_queryset())

        return context


class InteractionDetail(LoginRequiredMixin, MenuMixin, DetailView):
    """Класс для обработки запросов детального отображения взаимодействия

        Наследует:
                LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
                MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
                DetailView - класс джанго, для наследования функций для упращенной работы с одним объектом модели

        Atributes:
            model - указываеться класс модель список объекта которого хотим получить

        """
    model = Interaction

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['page_name'] = "Страница взаимодействия"
        return context


class InteractionCreateForm(LoginRequiredMixin, MenuMixin, TemplateView):
    """ Класс для обработки запросов создания взаимодействия.

        Наследует:
            LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
            MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
            TemplateView - класс джанго, для наследования общих функций и функцию отображения шаблона

        Atributes:
            form - класс формы которая будет работать с моделью Interaction
            template_name - переменная содержащая название шаблона для отображения


        """
    form = None
    template_name = 'crm/interaction_form.html'

    def post(self, request, *args, **kwargs):
        """Переопределенный метод для обработки запросов на создание взаимодействия посредством формы

        После успешной валидации формы, дополняет созданный на её основе объект даннымы о пользователе и проекте
        к которому принадлежит

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.form = InteractionForm(request.POST)
        if self.form.is_valid():

            # Черновое сохранение нового объекта взаимодействия
            interaction = self.form.save(commit=False)

            # Дополнение нового объекта взаимодействия данными о проекте к которому он относиться
            interaction.project = Project.objects.get(pk=self.kwargs['pk'])

            # Дополнение нового объекта взаимодействия данными о пользователе которым был создан
            interaction.manager = User.objects.get(id=request.user.pk)

            # Сохранение в базу
            interaction.save()

            messages.success(self.request, 'Взаимодействие успешно созданно!')
            return redirect(reverse('project-interaction-list', kwargs={'project_id': interaction.project.pk}))
        else:
            return super(InteractionCreateForm, self).get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """ Переопределение метода для обработки запросов на отображение формы для создания взаимодействия

        Создает форму для дольнейшого её добавления в шаблон

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        self.form = InteractionForm()
        return super(InteractionCreateForm, self).get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Переопределение метода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1
        Добавляет в контекст форму для отображения её в шаблоне
        Добавляет в шаблон оъбекта модели Project для понимания к чему будет отноститься созданный объект взаимодействия

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Создание '
        context['form'] = self.form
        context['project'] = Project.objects.get(pk=self.kwargs['pk'])
        return context


class InteractionUpdateForm(LoginRequiredMixin, MenuMixin, TemplateView):
    """ Класс для обработки запросов изменения взаимодействия.

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        TemplateView - класс джанго, для наследования общих функций и функцию отображения шаблона

    Atributes:
        form - класс формы которая будет работать с моделью Interaction
        template_name - переменная содержащая название шаблона для отображения

    """

    form = None
    template_name = 'crm/interaction_form.html'

    def post(self, request, *args, **kwargs):
        """Переопределенный метод для обработки запросов на изменение взаимодействия посредством формы

        После успешной валидации формы, изменяет существующий объект даннымы отправленными в форме если запрос послан
        пользователем создавшего данный объект

        ToDo:
            *Добавить обработку принадлежности взаимодействия
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        interaction = Interaction.objects.get(pk=kwargs['pk'])
        kwargs['project'] = interaction.project

        # Если текущий пользователь - автор
        if interaction.manager == User.objects.get(id=request.user.pk):

            self.form = InteractionForm(request.POST)

            if self.form.is_valid() and self.form.has_changed():
                # НАЧАЛО изменение данных существующего объекта данными из формы
                interaction.chanel = self.form.cleaned_data['chanel']
                interaction.description = self.form.cleaned_data['description']
                interaction.rate = self.form.cleaned_data['rate']
                # КОНЕЦ изменение данных существующего объекта данными из формы

                interaction.save()

                messages.success(self.request, 'Взаимодействие успешно обновленно!')
                return redirect(reverse('interaction', kwargs={'pk': interaction.pk}))
        # Если пользователь не автор взаимодействия получит сообщение
        messages.error(self.request, "Для данного действия необходимо быть автором")
        return super(InteractionUpdateForm, self).get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """Переопределение метода для обработки запросов на отображение страницы изменения объекта модели Interaction

        Создает форму на основе существующего обїекта для дальнейшего добавления её в контекст если пользователь
        являеться создателем изменяемого объекта

        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        interaction = Interaction.objects.get(pk=kwargs['pk'])
        kwargs['project'] = interaction.project

        if interaction.manager == User.objects.get(id=request.user.pk):  # Нужно добавить обработку
            self.form = InteractionForm(
                initial={
                    'chanel': interaction.chanel,
                    'description': interaction.description,
                    'rate': interaction.rate}
            )
            return super(InteractionUpdateForm, self).get(self, request, *args, **kwargs)

        # Если пользователь не автор взаимодействия получит сообщение
        messages.error(self.request, "Для данного действия необходимо быть автором")
        return super(InteractionUpdateForm, self).get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Переопределение метода для работы с контекстом

        Добавляет в контекст Текст для отображения в теге h1
        Добавляет в контекст форму для отображения её в шаблоне
        Добавляет в шаблон оъбекта модели Project для понимания к чему будет отноститься созданный объект взаимодействия

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)
        context['method'] = 'Изменение '
        context['form'] = self.form
        context['project'] = kwargs['project']
        return context


class InteractionDeleteForm(LoginRequiredMixin, DeleteView):
    """Класс предназанчений для обработки запросов на удаление объектов модели Interaction

    Atributes:
        model - класс модели удаляемого объекта
        template_name - переменная содержащая название шаблона для отображения
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном результате работы формы

    """
    model = Interaction
    template_name = 'crm/delete_form.html'
    success_url = reverse_lazy('interactions')

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет возвратную ссылку на страницу Взаимодействия

        :param kwargs:
        :return:
        """

        pk = self.object.pk  # Как это засунуть в кварг на след. строке?
        context = super().get_context_data(**kwargs)
        context['return'] = reverse_lazy('interaction', kwargs={'pk': pk})  # Как добавить ссылку назад
        return context

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успехе работы с формой и возврат на страницу Взаимодействий

        :return:
        """
        messages.success(self.request, 'Взаимодействие успешно удалено!')
        return super(InteractionDeleteForm, self).get_success_url()


class UpdateProfile(LoginRequiredMixin, MenuMixin, TemplateView):
    """ Класс для обработки запросов изменения профиля.

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        TemplateView - класс джанго, для наследования общих функций и функцию отображения шаблона

    Atributes:
        profile_form - класс формы которая будет работать с моделью Profile
        user_form - класс формы которая будет работать с моделью User
        template_name - переменная содержащая название шаблона для отображения

    """
    template_name = 'crm/profile.html'
    user_form = None
    profile_form = None

    def post(self, request, *arg, **kwargs):
        """Переопределенный метод отвчаюзий за обработку запросов изменения данных пользователя(User, Profile)
         посредством данных отправленных форм

        :param request:
        :param arg:
        :param kwargs:
        :return:
        """

        # НАЧАЛО Создание форм на основе существующих объектов и данных из присланных форм
        self.user_form = UserForm(request.POST, instance=request.user)
        self.profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        # КОНЕЦ Создание форм на основе существующих объектов и данных из присланных форм

        if self.user_form.is_valid() and self.profile_form.is_valid():

            self.user_form.save()
            self.profile_form.save()

            messages.success(request, 'Ваш профиль успешно обновлен!')
            return redirect('profile')
        else:
            messages.error(request, 'Не корректно введены данные')
        return super().post(request, *arg, **kwargs)

    def get(self, request, *arg, **kwargs):
        """Переопределенный метод отвечающий за обработку запросов на отображение страницы изменение объектов модели
        User, Profile

        Добавляет формы на основе существующих объектов для дальнейшего добавления их в контекст

        :param request:
        :param arg:
        :param kwargs:
        :return:
        """
        self.user_form = UserForm(instance=request.user)

        # Если аватар загружен сохраняем для дальнейшего добавления в контекст
        if request.user.profile.avatar:
            kwargs['avatar'] = request.user.profile.avatar

        self.profile_form = ProfileForm(instance=request.user.profile)
        return super().get(request, *arg, **kwargs)

    def get_context_data(self, **kwargs):
        """Переопределение мотода для работы с контекстом

        Добавляет в контект формы для работы с моделями User, Profile

        :param kwargs:
        :return:
        """
        context = super().get_context_data(**kwargs)

        context['user_form'] = self.user_form
        context['profile_form'] = self.profile_form

        if kwargs.get('avatar'):
            context['avatar'] = kwargs['avatar']
        return context


class UserPasswordChangeView(LoginRequiredMixin, MenuMixin, PasswordChangeView):
    """Класс для обработки запросов на измененеие пароля пользователя

    Наследует:
        LoginRequiredMixin - класс джанго, ограничивающий доступ не авторизированным пользователям
        MenuMixin - написанный класс, для добавления в контекст данные для генерации меню в хедере
        PasswordChangeView - класс джанго, для наследования общих функций по смене пароля пользователя

    Atributes:
        template_name - переменная содержащая название шаблона для отображения
        success_url - переменная хранящая Url по которой будет совершен редирект при успешном результате работы формы

    """
    template_name = 'crm/password_change.html'
    success_url = reverse_lazy('profile')

    def get_success_url(self):
        """Переопределеный метод для добавления сообщения об успешной смене пароля и возврат на страницу профиля

        :return:
        """

        messages.success(self.request, 'Ваш пароль успешно изменён!')
        return super(UserPasswordChangeView, self).get_success_url()
